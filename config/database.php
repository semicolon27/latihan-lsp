<?php

class Database{

    protected $host = "localhost";
    protected $dbname = "lsp";
    protected $user = "root";
    protected $pass = "";

    public function openDBConnection(){
        $options = array(  
            PDO::ATTR_PERSISTENT => true,  
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION  
        );
        $conn = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass, $options);
        return $conn;
    }

    public function closeDBConnection(&$conn){
        $conn = null;
    }
}

?>