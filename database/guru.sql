-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 09 Mar 2020 pada 06.14
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lsp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(4) NOT NULL,
  `nip` varchar(15) DEFAULT NULL,
  `nuptk` varchar(15) DEFAULT NULL,
  `nama_guru` varchar(25) NOT NULL,
  `idx_jk` int(1) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `email` varchar(15) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `tmp_lahir` varchar(15) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`id_guru`, `nip`, `nuptk`, `nama_guru`, `idx_jk`, `no_hp`, `email`, `tgl_lahir`, `tmp_lahir`, `alamat`) VALUES
(1, '198306262009022', '1', 'Jason', 1, '1', '1', '1980-03-10', 'Bandung', 'Jakarta'),
(2, NULL, '121212', 'Han Prakoso', 1, '083811605765', 'hanzpprakoso27@', '2002-09-27', 'Garut', 'Bogor'),
(3, NULL, '131313', 'Ahmad', 1, '083811605766', 'ahmad27@gmail.c', '2002-10-27', 'Bandung', 'Bogor'),
(4, NULL, '141414', 'Belinda', 0, '083811605767', 'belinda27@gmail', '2002-11-27', 'Bogor', 'Bogor'),
(5, NULL, '151515', 'Salsa Idha', 0, '083811605768', 'salsaidha27@gma', '2002-12-27', 'Depok', 'Bogor'),
(6, NULL, '161616', 'Siti Masitoh', 0, '083811605769', 'sitimasitoh27@g', '2002-01-27', 'Sukabumi', 'Bogor'),
(7, NULL, '171717', 'Remeyta', 0, '083811605760', 'remeyta27@gmail', '2002-02-27', 'Cianjur', 'Bogor');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`),
  ADD KEY `idx_jk` (`idx_jk`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
