<?php
require_once '../controllers/controllerJadwal.php';
require_once '../models/modelGuru.php';
$controller = new ControllerJadwal();
$mg = new ModelGuru();
$f = $_POST;

$listGuru = $mg->getIDNamaGuru();
$options = "";
$options .= "<option value='0' selected>Pilih guru</option>";
foreach($listGuru as $i => $v){
    $options .= "<option value='".$v["id_guru"]."'>".$v["nama_guru"]."</option>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Jadwal</title>
</head>
<body>
<?php
if(isset($f['submit']) && isset($_GET['kelas']) && isset($_GET['hari'])){
    $data =[
        ["mapel-1" => $f["mapel-1"], "guru-1" => $f["guru-1"]],
        ["mapel-2" => $f["mapel-2"], "guru-2" => $f["guru-2"]],
        ["mapel-3" => $f["mapel-3"], "guru-3" => $f["guru-3"]],
        ["mapel-4" => $f["mapel-4"], "guru-4" => $f["guru-4"]],
        ["mapel-5" => $f["mapel-5"], "guru-5" => $f["guru-5"]],
    ];
    $controller->postJadwal($data, $_GET['kelas'], $_GET['hari']);
}

?>
    <form name="form-jadwal" method="POST">
        <input type="text" name="mapel-1" placeholder="Mapel Jam ke-1">
        <select name="guru-1">
            <?php echo $options ?>
        </select>
        <br>
        <input type="text" name="mapel-2" placeholder="Mapel Jam ke-2">
        <select name="guru-2">
            <?php echo $options ?>
        </select>
        <br>
        <input type="text" name="mapel-3" placeholder="Mapel Jam ke-3">
        <select name="guru-3">
            <?php echo $options ?>
        </select>
        <br>
        <input type="text" name="mapel-4" placeholder="Mapel Jam ke-4">
        <select name="guru-4">
            <?php echo $options ?>
        </select>
        <br>
        <input type="text" name="mapel-5" placeholder="Mapel Jam ke-5">
        <select name="guru-5">
            <?php echo $options ?>
        </select>
        <br>
        <button type="submit" name="submit">submit</button>
    </form>
</body>
</html>