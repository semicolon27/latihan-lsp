<?php

require_once '../config/database.php';

class ModelGuru extends Database{

    public function getIDNamaGuru(){
        $con = $this->openDBConnection();
        $sql = "SELECT id_guru, nama_guru FROM guru ORDER BY nama_guru ASC";
        $result = $con->query($sql);
        return $result->fetchAll(PDO::FETCH_ASSOC);
        
    } 
}

?>